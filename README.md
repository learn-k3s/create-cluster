# Create K3S cluster

## Main commands

> prerequisites: you need to install Multipass (from Canonical): https://multipass.run/

- Create the cluster: `./create.cluster.sh`
- Stop the cluster: `./stop.cluster.sh`
- Start the cluster: `./start.cluster.sh`
- Remove the cluster: `./remove.cluster.sh`

## Kube commands (examples)

> prerequisites: you need to install KubeCtl

```shell
export KUBECONFIG=$PWD/k3s.yaml
kubectl top nodes
```

you'll get something like this:

```text
NAME         CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%   
basestar     101m         2%     702Mi           8%        
raider-one   15m          0%     267Mi           6%        
raider-two   19m          0%     265Mi           6%      
```