#!/bin/sh

echo "👋 restarting cluster 🚀"
./stop.cluster.sh
./start.cluster.sh
./update-coredns.sh
