#!/bin/sh
echo "👋 deleting cluster 😢"
eval $(cat cluster.config)

multipass delete ${node1_name}
multipass delete ${node2_name}
multipass delete ${node3_name}

rm k3s.yaml

multipass list
multipass purge

