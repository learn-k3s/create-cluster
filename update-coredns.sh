#!/bin/bash
export KUBECONFIG=$PWD/k3s.yaml
rm coredns.yaml
kubectl get configmap coredns -n kube-system -o yaml > coredns.yaml

corefilepatch=$(yq read coredns.patch.yaml  'data.Corefile')

yq delete -i coredns.yaml 'data.Corefile'
yq write -i coredns.yaml 'data.Corefile' "${corefilepatch}"

kubectl apply -f coredns.yaml
kubectl delete pod --selector=k8s-app=kube-dns -n kube-system
