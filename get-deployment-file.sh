#!/bin/bash

# Help
# get-deployment-file.sh amazing-web-app training

export KUBECONFIG=$PWD/k3s.yaml
deployment_name=$1
namespace=$2
kubectl get deployment ${deployment_name} -n ${namespace} -o yaml > ${deployment_name}.yaml

# kubectl get deployment amazing-web-app -n training -o yaml > amazing-web-app.yaml
