#!/bin/bash
export KUBECONFIG=$PWD/k3s.yaml
deployment_name=$1
namespace=$2
nb_replicas=$3

kubectl scale deploy ${deployment_name} --namespace="${namespace}" --replicas=${nb_replicas}

#kubectl scale deploy amazing-web-app --namespace="training" --replicas=3